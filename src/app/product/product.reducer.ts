import { Action, createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Product } from './product.model';
import * as ProductActions from './product.actions';

export const productsFeatureKey = 'products';

export interface State extends EntityState<Product> {
  entity?: Product;
}

export const adapter: EntityAdapter<Product> = createEntityAdapter<Product>();

export const initialState: State = adapter.getInitialState({
  // additional entity state properties
});

export const reducer = createReducer(
  initialState,
  on(ProductActions.updateProduct,
    (state, action) => adapter.updateOne(action.update, state)
  ),
  on(ProductActions.deleteProducts,
    (state, action) => adapter.removeMany(action.ids, state)
  ),
  on(ProductActions.productLoaded,
    (state, action) => {
      return { ...adapter.setOne(action.product, state), entity: action.product }
    }
  ),
  on(ProductActions.productsLoaded,
    (state, action) => adapter.setAll(action.products, state)
  ),
);

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();
