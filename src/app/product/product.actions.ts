import { createAction, props } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { Product } from './product.model';

export const loadProducts = createAction('[Product/API] Load Products');
export const productsLoaded = createAction('[Product/API] Products Loaded Successfully',
  props<{ products: Product[] }>());

export const loadProduct = createAction('[Product/API] Load Product', props<{ id: string }>());
export const productLoaded = createAction('[Product/API] Product Loaded Successfully',
  props<{ product: Product }>());

export const createProduct = createAction('[Product/API] Create Product',
  props<{ product: Product }>());

export const deleteProducts = createAction('[Product/API] Delete Products',
  props<{ ids: string[] }>());

export const updateProduct = createAction('[Product/API] Update Product',
  props<{ update: Update<Product> }>());

export const productActionTypes = {
  loadProducts,
  productsLoaded,
  loadProduct,
  productLoaded,
  createProduct,
  deleteProducts,
  updateProduct,
};
