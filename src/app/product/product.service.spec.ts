import { TestBed } from '@angular/core/testing';

import { ProductService } from './product.service';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { SharedModule } from '../shared/shared.module';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('ProductService', () => {
  let service: ProductService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers), HttpClientTestingModule],
    });
    service = TestBed.inject(ProductService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  const products = [
        {
          id: '1',
          name: 'Malayan Tapir',
          code: 'Malayan Tapir',
        },
        {
          id: '2',
          name: 'Malayan Tapir',
          code: 'Malayan Tapir',
        }
  ];

  describe('#getAll', () => {
    it('should return an Observable<Product[]>', () => {
      service.getAll().subscribe((result) => {
        expect(result.length).toBe(2);
        expect(result).toEqual(result);
      });

      const req = httpMock.expectOne(`/api/products`);
      expect(req.request.method).toBe('GET');
      req.flush(products);
    });
  });

  describe('#getOne', () => {
    it('should return an Observable<Product>', () => {
      service.getOne('42').subscribe((result) => {
        expect(result).toBeTruthy();
        expect(result).toEqual(result);
      });

      const req = httpMock.expectOne(`/api/products/42`);
      expect(req.request.method).toBe('GET');
      req.flush(products);
    });
  });

  describe('#create', () => {
    it('should return an Observable<Product>', () => {
      service.create(products[0]).subscribe((result) => {
        expect(result).toBeTruthy();
        expect(result).toEqual(result);
      });

      const req = httpMock.expectOne(`/api/products`);
      expect(req.request.method).toBe('POST');
      req.flush(products);
    });
  });

  describe('#delete', () => {
    it('should return an Observable<Product>', () => {
      service.delete(['42']).subscribe((result) => {
        expect(result).toBeTruthy();
        expect(result).toEqual(result);
      });

      const req = httpMock.expectOne(`/api/products/42`);
      expect(req.request.method).toBe('DELETE');
      req.flush(products);
    });
  });

  describe('#update', () => {
    it('should return an Observable<Product>', () => {
      service.update({id: '42', changes: {}}).subscribe((result) => {
        expect(result).toBeTruthy();
        expect(result).toEqual(result);
      });

      const req = httpMock.expectOne(`/api/products/42`);
      expect(req.request.method).toBe('PUT');
      req.flush(products);
    });
  });

});
