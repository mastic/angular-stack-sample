import { State } from './product.reducer';
import { selectAllProducts } from './product.selectors';

const state: State = {
  ids: ['1', '2', '3'],
  entities: {
    1: { id: '1',
           name: 'Malayan Tapir',
           code: 'Malayan Tapir',
      },
      2: { id: '2',
           name: 'Malayan Tapir',
           code: 'Malayan Tapir',
      },
      3: { id: '3',
           name: 'Malayan Tapir',
           code: 'Malayan Tapir',
      },
  },
};

describe('Product Selectors', () => {
  it('selectAllProducts', () => {
    expect(selectAllProducts.projector(state)).toEqual([
      {
        id: '1',
        name: 'Malayan Tapir',
        code: 'Malayan Tapir',
      },
      {
        id: '2',
        name: 'Malayan Tapir',
        code: 'Malayan Tapir',
      },
      {
        id: '3',
        name: 'Malayan Tapir',
        code: 'Malayan Tapir',
      }
    ]);
  });
});
