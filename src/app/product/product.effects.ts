import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ProductService } from './product.service';
import {
  createProduct, productLoaded, productsLoaded,
  deleteProducts, loadProduct, loadProducts, updateProduct
} from './product.actions';
import { concatMap, map } from 'rxjs/operators';

@Injectable()
export class ProductEffects {
  constructor(private service: ProductService, private actions$: Actions) { }

  loadProducts$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadProducts),
      concatMap(() => this.service.getAll()),
      map((products) => productsLoaded({ products }))
    )
  );

  loadProduct$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadProduct),
      concatMap((action) => this.service.getOne(action.id)),
      map((product) => productLoaded({ product }))
    )
  );

  createProduct$ = createEffect(() =>
    this.actions$.pipe(
      ofType(createProduct),
      concatMap((action) => this.service.create(action.product)),
      map((product) => loadProducts())
    )
  );

  updateProduct$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateProduct),
      concatMap((action) => this.service.update(action.update)),
      map((product) => loadProducts())
    )
  );

  deleteProducts$ = createEffect(() =>
    this.actions$.pipe(
      ofType(deleteProducts),
      concatMap((action) => this.service.delete(action.ids)),
      map((product) => loadProducts())
    )
  );
}
