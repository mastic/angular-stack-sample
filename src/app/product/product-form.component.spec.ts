import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProductFormComponent } from './product-form.component';
import { SharedModule } from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { RouterModule } from '@angular/router';

const createNewEvent = (eventName: string, bubbles = false, cancelable = false): CustomEvent => {
  const evt = document.createEvent('CustomEvent');
  evt.initCustomEvent(eventName, bubbles, cancelable, null);
  return evt;
};

describe('ProductFormComponent', () => {
  let component: ProductFormComponent;
  let fixture: ComponentFixture<ProductFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers), RouterModule.forRoot([], { relativeLinkResolution: 'legacy' })],
      declarations: [ProductFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have name', () => {
    expect(component.name).toBeTruthy();
  });

  it('should have code', () => {
    expect(component.code).toBeTruthy();
  });

  it('initially should be invalid', () => {
    expect(component.productForm.valid).toBeFalsy();
  });

  it('initially should be valid upon input', () => {
    const event = createNewEvent('input');

    const name = fixture.nativeElement.querySelector('#name');
    name.value = 'Malayan Tapir';
    name.dispatchEvent(event);

    const code = fixture.nativeElement.querySelector('#code');
    code.value = 'Malayan Tapir';
    code.dispatchEvent(event);

    expect(component.productForm.valid).toBeTruthy();
  });
});
