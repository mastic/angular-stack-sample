import { reducer, initialState } from './product.reducer';
import {
  createProduct,
  updateProduct,
  deleteProducts,
  productLoaded,
  productsLoaded
} from './product.actions';

describe('Product Reducer', () => {
  describe('unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;
      const result = reducer(initialState, action);
      expect(result).toBe(initialState);
    });
  });

  describe('updateProduct', () => {
    it('should not update missing one', () => {
      const action = updateProduct({ update: { id: '1', changes: {
        name: 'Malayan Tapir Updated',
        code: 'Malayan Tapir Updated',
      } } });
      const result = reducer(initialState, action);
      expect(result).toEqual({ ids: [], entities: {} });
    });

    it('should update one', () => {
      const product = {
        id: '1',
        name: 'Malayan Tapir',
        code: 'Malayan Tapir',
      };
      const action = updateProduct({ update: { id: '1', changes: {
        name: 'Malayan Tapir Updated',
        code: 'Malayan Tapir Updated',
      } } });
      const result = reducer({ ids: ['1'], entities: { ['1']: product } }, action);
      expect(result.ids).toEqual(['1']);
      expect(result.entities['1']).toEqual({
        id: '1',
        name: 'Malayan Tapir Updated',
        code: 'Malayan Tapir Updated',
      });
    });
  });

  describe('deleteProducts', () => {
    it('should delete one', () => {
      const product = {
        id: '1',
        name: 'Malayan Tapir',
        code: 'Malayan Tapir',
      };
      const action = deleteProducts({ ids: ['1'] });
      const result = reducer({ ids: ['1'], entities: { ['1']: product } }, action);
      expect(result.ids).toEqual([]);
    });
  });

  describe('productLoaded', () => {
    it('should set one', () => {
      const product = {
        id: '1',
        name: 'Malayan Tapir',
        code: 'Malayan Tapir',
      };
      const action = productLoaded({ product });
      const result = reducer(initialState, action);
      expect(result.ids).toEqual(['1']);
    });
  });

  describe('productsLoaded', () => {
    it('should sets all', () => {
      const products = [
        {
          id: '1',
          name: 'Malayan Tapir',
          code: 'Malayan Tapir',
        },
        {
          id: '2',
          name: 'Malayan Tapir',
          code: 'Malayan Tapir',
        }
      ];
      const action = productsLoaded({ products });
      const result = reducer(initialState, action);
      expect(result.ids).toEqual(['1', '2']);
    });
  });
});
