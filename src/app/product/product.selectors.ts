import { createFeatureSelector, createSelector } from '@ngrx/store';
import { productsFeatureKey, selectAll, State } from './product.reducer';

const productFeatureSelector = createFeatureSelector<State>(productsFeatureKey);
export const selectAllProducts = createSelector(productFeatureSelector, selectAll);
export const selectCurrentProduct = createSelector(productFeatureSelector,
    s => s.entity,
);
