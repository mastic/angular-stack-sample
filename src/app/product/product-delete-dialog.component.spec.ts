import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProductDeleteDialogComponent } from './product-delete-dialog.component';
import { SharedModule } from '../shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

describe('ProductDeleteDialogComponent', () => {
  let component: ProductDeleteDialogComponent;
  let fixture: ComponentFixture<ProductDeleteDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ProductDeleteDialogComponent],
      imports: [SharedModule, BrowserAnimationsModule],
      providers: [
        { provide: MatDialogRef, useValue: {} },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            selection: [{
              id: '1',
              name: 'Malayan Tapir',
              code: 'Malayan Tapir',
            }],
          },
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductDeleteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have data', () => {
    expect(component.data).toBeTruthy();
    expect(component.data.selection).toBeTruthy();
    expect(component.data.selection.length).toEqual(1);
  });

});
