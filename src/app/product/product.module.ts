import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './product.component';
import { ProductUpdateComponent } from './product-update.component';
import { ProductDeleteDialogComponent } from './product-delete-dialog.component';
import { StoreModule } from '@ngrx/store';
import * as fromProduct from './product.reducer';
import { SharedModule } from '../shared/shared.module';
import { ProductFormComponent } from './product-form.component';
import { EffectsModule } from '@ngrx/effects';
import { ProductEffects } from './product.effects';

@NgModule({
  declarations: [
    ProductComponent,
    ProductFormComponent,
    ProductUpdateComponent,
    ProductDeleteDialogComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    StoreModule.forFeature(fromProduct.productsFeatureKey, fromProduct.reducer),
    EffectsModule.forFeature([ProductEffects])
  ]
})
export class ProductModule { }
