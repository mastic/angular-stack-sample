import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Product } from './product.model';
import { Store } from '@ngrx/store';
import { State } from '../reducers';
import { selectCurrentProduct } from './product.selectors';
import { loadProduct } from './product.actions';
import { notNull } from '../reducers/common';

@Component({
  selector: 'app-product-update',
  templateUrl: './product-update.component.html',
  styleUrls: ['./product-update.component.sass'],
})
export class ProductUpdateComponent implements OnInit {
  product$: Observable<Product>;

  constructor(private store: Store<State>, private route: ActivatedRoute) {
    this.route.params.pipe(map((params) => params['key'])).subscribe((id) => {
      this.store.dispatch(loadProduct({ id }));
    });
    this.product$ = this.store.select(selectCurrentProduct)
      .pipe(notNull());
  }

  ngOnInit(): void {}
}

