import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Product } from './product.model';
import { selectAllProducts } from './product.selectors';
import { State } from '../reducers';
import { createProduct, deleteProducts, loadProducts } from './product.actions';
import { v4 as uuid } from 'uuid';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { ProductDeleteDialogComponent } from './product-delete-dialog.component';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.sass'],
})
export class ProductComponent implements OnInit {
  public dataSource: Observable<Product[]>;
  public displayedColumns: string[] = [
    'id',
    'delete',
    'name',
    'code',
  ];
  public selection = new SelectionModel<Product>(true, []);

  constructor(private store: Store<State>, private dialog: MatDialog) {
    this.dataSource = this.store.select(selectAllProducts);
  }

  ngOnInit(): void {
    this.store.dispatch(loadProducts());
  }

  onDeleteClick(event: Event): void {
    // prevent parent event happening (navigation)
    event.preventDefault();
    event.stopImmediatePropagation();

    const dialogRef = this.dialog.open(ProductDeleteDialogComponent, {
      data: {
        selection: this.selection.selected,
      },
    });
    dialogRef.afterClosed().subscribe((result: Product[]) => {
      const ids = result.map((entity) => entity.id);
      if (ids && ids.length) {
        this.store.dispatch(deleteProducts({ ids }));
        this.selection.clear();
      }
    });
  }

  toggleSelectAll(): void {
    if (this.selection.hasValue()) {
      this.selection.clear();
    } else {
      this.dataSource.forEach((rows) => rows.forEach((row) => this.selection.select(row)));
    }
  }

  create(entity: Partial<Product>): void {
    const product: Product = {
      id: uuid(),
      name: entity.name!,
      code: entity.code!,
    };
    this.store.dispatch(createProduct({ product }));
  }
}

