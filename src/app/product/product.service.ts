import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, forkJoin } from 'rxjs';
import { Product } from './product.model';
import { Update } from '@ngrx/entity';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  http: HttpClient;

  constructor(http: HttpClient) {
    this.http = http;
  }

  getAll(): Observable<Product[]> {
    return this.http.get<Product[]>(`/api/products`);
  }

  getOne(key: string): Observable<Product> {
    return this.http.get<Product>(`/api/products/${key}`);
  }

  create(product: Product): Observable<Product> {
    return this.http.post<Product>(`/api/products`, product);
  }

  delete(keys: string[]): Observable<any> {
    return forkJoin(keys.map(key => this.http.delete(`/api/products/${key}`)));
  }

  update(update: Update<Product>): Observable<any> {
    return this.http.put(`/api/products/${update.id}`, update.changes);
  }
}
