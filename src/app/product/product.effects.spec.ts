import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable, throwError, of } from 'rxjs';
import { cold } from 'jasmine-marbles';

import { ProductEffects } from './product.effects';
import {
  loadProduct,
  loadProducts,
  productLoaded,
  productsLoaded,
  createProduct,
  updateProduct,
  deleteProducts,
} from './product.actions';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { SharedModule } from '../shared/shared.module';
import { Actions } from '@ngrx/effects';

const serviceStub = (method: string, response: any) => {
  const service = jasmine.createSpyObj('product', [method]);
  const isError = response instanceof Error;
  const serviceResponse = isError ? throwError(response) : of(response);
  service[method].and.returnValue(serviceResponse);
  return service;
};

describe('ProductEffects', () => {
  // tslint:disable-next-line: prefer-const
  let actions$: Observable<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers)],
      providers: [ProductEffects, provideMockActions(() => actions$)],
    });
  });

  it('should be created', () => {
    const effects = TestBed.inject(ProductEffects);
    expect(effects).toBeTruthy();
  });

  it('loadProducts should return productsLoaded', () => {
    const source = cold('a', { a: loadProducts() });
    const service = serviceStub('getAll', []);
    const effects = new ProductEffects(service, new Actions(source));

    const expected = cold('a', { a: productsLoaded({ products: [] }) });
    expect(effects.loadProducts$).toBeObservable(expected);
  });

  it('loadProduct should return productLoaded', () => {
    const source = cold('a', { a: loadProduct({ id: '42' }) });
    const product = {
      id: '1',
      name: 'Malayan Tapir',
      code: 'Malayan Tapir',
    };
    const service = serviceStub('getOne', product);
    const effects = new ProductEffects(service, new Actions(source));

    const expected = cold('a', { a: productLoaded({ product }) });
    expect(effects.loadProduct$).toBeObservable(expected);
  });

  it('createProduct should return productLoaded', () => {
    const product = {
      id: '1',
      name: 'Malayan Tapir',
      code: 'Malayan Tapir',
    };
    const source = cold('a', { a: createProduct({ product }) });
    const service = serviceStub('create', product);
    const effects = new ProductEffects(service, new Actions(source));

    const expected = cold('a', { a: loadProducts() });
    expect(effects.createProduct$).toBeObservable(expected);
  });

  it('updateProduct should return loadProducts', () => {
    const source = cold('a', { a: updateProduct({ update: { id: '1', changes: {} } }) });
    const product = {
      id: '1',
      name: 'Malayan Tapir',
      code: 'Malayan Tapir',
    };
    const service = serviceStub('update', product);
    const effects = new ProductEffects(service, new Actions(source));

    const expected = cold('a', { a: loadProducts() });
    expect(effects.updateProduct$).toBeObservable(expected);
  });

  it('deleteProducts should return loadProducts', () => {
    const source = cold('a', { a: deleteProducts({ ids: ['1'] }) });
    const product = {
      id: '1',
      name: 'Malayan Tapir',
      code: 'Malayan Tapir',
    };
    const service = serviceStub('delete', product);
    const effects = new ProductEffects(service, new Actions(source));

    const expected = cold('a', { a: loadProducts() });
    expect(effects.deleteProducts$).toBeObservable(expected);
  });
});
