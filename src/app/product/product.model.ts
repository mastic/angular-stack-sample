// mastic-pin-import-statements

export interface Product {
  id: string;
  name: string;
  code: string;
}
