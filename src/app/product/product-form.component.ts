import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { State } from '../reducers';
import { createProduct, updateProduct } from './product.actions';
import { Observable } from 'rxjs';
import { Product } from './product.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.sass'],
})
export class ProductFormComponent implements OnInit {
  @Input() product!: Observable<Product>;

  productForm = new FormGroup({
    id: new FormControl(''),
    name: new FormControl(''),
    code: new FormControl(''),
  });

  constructor(private store: Store<State>, private router: Router) {}

  ngOnInit(): void {
    if (this.product) {
      this.product.subscribe((product) => {
        if (product && product.id) {
          this.productForm.setValue(product);
        }
      });
    }
  }

  get name(): FormControl {
    return this.productForm.get('name') as FormControl;
  }

  get code(): FormControl {
    return this.productForm.get('code') as FormControl;
  }

  onSubmit(): void {
    if (this.productForm.valid) {
      if (this.productForm.value.id) {
        this.store.dispatch(
          updateProduct({
            update: {
              id: this.productForm.value.id,
              changes: this.productForm.value,
            },
          })
        );
        this.router.navigate(['/product']);
      } else {
        const value = this.productForm.value;
        const { id, ...product } = value;
        this.store.dispatch(createProduct({ product }));
      }
    }
  }
}
