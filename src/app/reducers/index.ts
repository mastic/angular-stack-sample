import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { environment } from '../../environments/environment';
import { debug } from './debug';
import * as fromCustomer from '../customer/customer.reducer';
import * as fromOrder from '../order/order.reducer';
import * as fromProduct from '../product/product.reducer';
import * as fromOrderItem from '../order-item/order-item.reducer';

export interface State {
  [fromCustomer.customersFeatureKey]: fromCustomer.State;
  [fromOrder.ordersFeatureKey]: fromOrder.State;
  [fromProduct.productsFeatureKey]: fromProduct.State;
  [fromOrderItem.orderItemsFeatureKey]: fromOrderItem.State;
}

export const reducers: ActionReducerMap<State> = {
  [fromCustomer.customersFeatureKey]: fromCustomer.reducer,
  [fromOrder.ordersFeatureKey]: fromOrder.reducer,
  [fromProduct.productsFeatureKey]: fromProduct.reducer,
  [fromOrderItem.orderItemsFeatureKey]: fromOrderItem.reducer,
};

export const metaReducers: MetaReducer<State>[] = !environment.production ? [debug] : [];
