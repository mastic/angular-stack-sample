import { filter, OperatorFunction } from "rxjs";

export function notNull<T>(): OperatorFunction<T | undefined, T> {
  return filter(x => x != undefined) as OperatorFunction<T | null | undefined, T>;
}
