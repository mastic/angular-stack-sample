import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Customer } from './customer.model';
import { Store } from '@ngrx/store';
import { State } from '../reducers';
import { selectCurrentCustomer } from './customer.selectors';
import { loadCustomer } from './customer.actions';
import { notNull } from '../reducers/common';

@Component({
  selector: 'app-customer-update',
  templateUrl: './customer-update.component.html',
  styleUrls: ['./customer-update.component.sass'],
})
export class CustomerUpdateComponent implements OnInit {
  customer$: Observable<Customer>;

  constructor(private store: Store<State>, private route: ActivatedRoute) {
    this.route.params.pipe(map((params) => params['key'])).subscribe((id) => {
      this.store.dispatch(loadCustomer({ id }));
    });
    this.customer$ = this.store.select(selectCurrentCustomer)
      .pipe(notNull());
  }

  ngOnInit(): void {}
}

