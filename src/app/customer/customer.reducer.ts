import { Action, createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Customer } from './customer.model';
import * as CustomerActions from './customer.actions';

export const customersFeatureKey = 'customers';

export interface State extends EntityState<Customer> {
  entity?: Customer;
}

export const adapter: EntityAdapter<Customer> = createEntityAdapter<Customer>();

export const initialState: State = adapter.getInitialState({
  // additional entity state properties
});

export const reducer = createReducer(
  initialState,
  on(CustomerActions.updateCustomer,
    (state, action) => adapter.updateOne(action.update, state)
  ),
  on(CustomerActions.deleteCustomers,
    (state, action) => adapter.removeMany(action.ids, state)
  ),
  on(CustomerActions.customerLoaded,
    (state, action) => {
      return { ...adapter.setOne(action.customer, state), entity: action.customer }
    }
  ),
  on(CustomerActions.customersLoaded,
    (state, action) => adapter.setAll(action.customers, state)
  ),
);

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();
