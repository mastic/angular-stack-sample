import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { State } from '../reducers';
import { createCustomer, updateCustomer } from './customer.actions';
import { Observable } from 'rxjs';
import { Customer } from './customer.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.sass'],
})
export class CustomerFormComponent implements OnInit {
  @Input() customer!: Observable<Customer>;

  customerForm = new FormGroup({
    id: new FormControl(''),
    name: new FormControl(''),
  });

  constructor(private store: Store<State>, private router: Router) {}

  ngOnInit(): void {
    if (this.customer) {
      this.customer.subscribe((customer) => {
        if (customer && customer.id) {
          this.customerForm.setValue(customer);
        }
      });
    }
  }

  get name(): FormControl {
    return this.customerForm.get('name') as FormControl;
  }

  onSubmit(): void {
    if (this.customerForm.valid) {
      if (this.customerForm.value.id) {
        this.store.dispatch(
          updateCustomer({
            update: {
              id: this.customerForm.value.id,
              changes: this.customerForm.value,
            },
          })
        );
        this.router.navigate(['/customer']);
      } else {
        const value = this.customerForm.value;
        const { id, ...customer } = value;
        this.store.dispatch(createCustomer({ customer }));
      }
    }
  }
}
