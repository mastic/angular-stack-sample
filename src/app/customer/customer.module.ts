import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerComponent } from './customer.component';
import { CustomerUpdateComponent } from './customer-update.component';
import { CustomerDeleteDialogComponent } from './customer-delete-dialog.component';
import { StoreModule } from '@ngrx/store';
import * as fromCustomer from './customer.reducer';
import { SharedModule } from '../shared/shared.module';
import { CustomerFormComponent } from './customer-form.component';
import { EffectsModule } from '@ngrx/effects';
import { CustomerEffects } from './customer.effects';

@NgModule({
  declarations: [
    CustomerComponent,
    CustomerFormComponent,
    CustomerUpdateComponent,
    CustomerDeleteDialogComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    StoreModule.forFeature(fromCustomer.customersFeatureKey, fromCustomer.reducer),
    EffectsModule.forFeature([CustomerEffects])
  ]
})
export class CustomerModule { }
