import { TestBed } from '@angular/core/testing';

import { CustomerService } from './customer.service';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { SharedModule } from '../shared/shared.module';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('CustomerService', () => {
  let service: CustomerService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers), HttpClientTestingModule],
    });
    service = TestBed.inject(CustomerService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  const customers = [
        {
          id: '1',
          name: 'Malayan Tapir',
        },
        {
          id: '2',
          name: 'Malayan Tapir',
        }
  ];

  describe('#getAll', () => {
    it('should return an Observable<Customer[]>', () => {
      service.getAll().subscribe((result) => {
        expect(result.length).toBe(2);
        expect(result).toEqual(result);
      });

      const req = httpMock.expectOne(`/api/customers`);
      expect(req.request.method).toBe('GET');
      req.flush(customers);
    });
  });

  describe('#getOne', () => {
    it('should return an Observable<Customer>', () => {
      service.getOne('42').subscribe((result) => {
        expect(result).toBeTruthy();
        expect(result).toEqual(result);
      });

      const req = httpMock.expectOne(`/api/customers/42`);
      expect(req.request.method).toBe('GET');
      req.flush(customers);
    });
  });

  describe('#create', () => {
    it('should return an Observable<Customer>', () => {
      service.create(customers[0]).subscribe((result) => {
        expect(result).toBeTruthy();
        expect(result).toEqual(result);
      });

      const req = httpMock.expectOne(`/api/customers`);
      expect(req.request.method).toBe('POST');
      req.flush(customers);
    });
  });

  describe('#delete', () => {
    it('should return an Observable<Customer>', () => {
      service.delete(['42']).subscribe((result) => {
        expect(result).toBeTruthy();
        expect(result).toEqual(result);
      });

      const req = httpMock.expectOne(`/api/customers/42`);
      expect(req.request.method).toBe('DELETE');
      req.flush(customers);
    });
  });

  describe('#update', () => {
    it('should return an Observable<Customer>', () => {
      service.update({id: '42', changes: {}}).subscribe((result) => {
        expect(result).toBeTruthy();
        expect(result).toEqual(result);
      });

      const req = httpMock.expectOne(`/api/customers/42`);
      expect(req.request.method).toBe('PUT');
      req.flush(customers);
    });
  });

});
