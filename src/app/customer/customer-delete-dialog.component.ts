import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Customer } from './customer.model';

@Component({
  selector: 'app-customer-delete-dialog',
  templateUrl: './customer-delete-dialog.component.html',
  styleUrls: ['./customer-delete-dialog.component.sass'],
})
export class CustomerDeleteDialogComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {selection: Customer[]},
    public dialogRef: MatDialogRef<CustomerDeleteDialogComponent>
  ) {}

  ngOnInit(): void {}
}
