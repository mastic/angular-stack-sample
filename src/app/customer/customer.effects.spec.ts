import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable, throwError, of } from 'rxjs';
import { cold } from 'jasmine-marbles';

import { CustomerEffects } from './customer.effects';
import {
  loadCustomer,
  loadCustomers,
  customerLoaded,
  customersLoaded,
  createCustomer,
  updateCustomer,
  deleteCustomers,
} from './customer.actions';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { SharedModule } from '../shared/shared.module';
import { Actions } from '@ngrx/effects';

const serviceStub = (method: string, response: any) => {
  const service = jasmine.createSpyObj('customer', [method]);
  const isError = response instanceof Error;
  const serviceResponse = isError ? throwError(response) : of(response);
  service[method].and.returnValue(serviceResponse);
  return service;
};

describe('CustomerEffects', () => {
  // tslint:disable-next-line: prefer-const
  let actions$: Observable<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers)],
      providers: [CustomerEffects, provideMockActions(() => actions$)],
    });
  });

  it('should be created', () => {
    const effects = TestBed.inject(CustomerEffects);
    expect(effects).toBeTruthy();
  });

  it('loadCustomers should return customersLoaded', () => {
    const source = cold('a', { a: loadCustomers() });
    const service = serviceStub('getAll', []);
    const effects = new CustomerEffects(service, new Actions(source));

    const expected = cold('a', { a: customersLoaded({ customers: [] }) });
    expect(effects.loadCustomers$).toBeObservable(expected);
  });

  it('loadCustomer should return customerLoaded', () => {
    const source = cold('a', { a: loadCustomer({ id: '42' }) });
    const customer = {
      id: '1',
      name: 'Malayan Tapir',
    };
    const service = serviceStub('getOne', customer);
    const effects = new CustomerEffects(service, new Actions(source));

    const expected = cold('a', { a: customerLoaded({ customer }) });
    expect(effects.loadCustomer$).toBeObservable(expected);
  });

  it('createCustomer should return customerLoaded', () => {
    const customer = {
      id: '1',
      name: 'Malayan Tapir',
    };
    const source = cold('a', { a: createCustomer({ customer }) });
    const service = serviceStub('create', customer);
    const effects = new CustomerEffects(service, new Actions(source));

    const expected = cold('a', { a: loadCustomers() });
    expect(effects.createCustomer$).toBeObservable(expected);
  });

  it('updateCustomer should return loadCustomers', () => {
    const source = cold('a', { a: updateCustomer({ update: { id: '1', changes: {} } }) });
    const customer = {
      id: '1',
      name: 'Malayan Tapir',
    };
    const service = serviceStub('update', customer);
    const effects = new CustomerEffects(service, new Actions(source));

    const expected = cold('a', { a: loadCustomers() });
    expect(effects.updateCustomer$).toBeObservable(expected);
  });

  it('deleteCustomers should return loadCustomers', () => {
    const source = cold('a', { a: deleteCustomers({ ids: ['1'] }) });
    const customer = {
      id: '1',
      name: 'Malayan Tapir',
    };
    const service = serviceStub('delete', customer);
    const effects = new CustomerEffects(service, new Actions(source));

    const expected = cold('a', { a: loadCustomers() });
    expect(effects.deleteCustomers$).toBeObservable(expected);
  });
});
