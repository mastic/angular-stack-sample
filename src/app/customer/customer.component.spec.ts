import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CustomerComponent } from './customer.component';
import { SharedModule } from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { CustomerFormComponent } from './customer-form.component';
import { RouterModule } from '@angular/router';

describe('CustomerComponent', () => {
  let component: CustomerComponent;
  let fixture: ComponentFixture<CustomerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers), RouterModule.forRoot([], { relativeLinkResolution: 'legacy' })],
      declarations: [CustomerComponent, CustomerFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
