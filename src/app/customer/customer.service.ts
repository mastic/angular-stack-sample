import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, forkJoin } from 'rxjs';
import { Customer } from './customer.model';
import { Update } from '@ngrx/entity';

@Injectable({
  providedIn: 'root',
})
export class CustomerService {
  http: HttpClient;

  constructor(http: HttpClient) {
    this.http = http;
  }

  getAll(): Observable<Customer[]> {
    return this.http.get<Customer[]>(`/api/customers`);
  }

  getOne(key: string): Observable<Customer> {
    return this.http.get<Customer>(`/api/customers/${key}`);
  }

  create(customer: Customer): Observable<Customer> {
    return this.http.post<Customer>(`/api/customers`, customer);
  }

  delete(keys: string[]): Observable<any> {
    return forkJoin(keys.map(key => this.http.delete(`/api/customers/${key}`)));
  }

  update(update: Update<Customer>): Observable<any> {
    return this.http.put(`/api/customers/${update.id}`, update.changes);
  }
}
