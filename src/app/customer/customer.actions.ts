import { createAction, props } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { Customer } from './customer.model';

export const loadCustomers = createAction('[Customer/API] Load Customers');
export const customersLoaded = createAction('[Customer/API] Customers Loaded Successfully',
  props<{ customers: Customer[] }>());

export const loadCustomer = createAction('[Customer/API] Load Customer', props<{ id: string }>());
export const customerLoaded = createAction('[Customer/API] Customer Loaded Successfully',
  props<{ customer: Customer }>());

export const createCustomer = createAction('[Customer/API] Create Customer',
  props<{ customer: Customer }>());

export const deleteCustomers = createAction('[Customer/API] Delete Customers',
  props<{ ids: string[] }>());

export const updateCustomer = createAction('[Customer/API] Update Customer',
  props<{ update: Update<Customer> }>());

export const customerActionTypes = {
  loadCustomers,
  customersLoaded,
  loadCustomer,
  customerLoaded,
  createCustomer,
  deleteCustomers,
  updateCustomer,
};
