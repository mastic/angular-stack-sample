import { createFeatureSelector, createSelector } from '@ngrx/store';
import { customersFeatureKey, selectAll, State } from './customer.reducer';

const customerFeatureSelector = createFeatureSelector<State>(customersFeatureKey);
export const selectAllCustomers = createSelector(customerFeatureSelector, selectAll);
export const selectCurrentCustomer = createSelector(customerFeatureSelector,
    s => s.entity,
);
