import { State } from './customer.reducer';
import { selectAllCustomers } from './customer.selectors';

const state: State = {
  ids: ['1', '2', '3'],
  entities: {
    1: { id: '1',
           name: 'Malayan Tapir',
      },
      2: { id: '2',
           name: 'Malayan Tapir',
      },
      3: { id: '3',
           name: 'Malayan Tapir',
      },
  },
};

describe('Customer Selectors', () => {
  it('selectAllCustomers', () => {
    expect(selectAllCustomers.projector(state)).toEqual([
      {
        id: '1',
        name: 'Malayan Tapir',
      },
      {
        id: '2',
        name: 'Malayan Tapir',
      },
      {
        id: '3',
        name: 'Malayan Tapir',
      }
    ]);
  });
});
