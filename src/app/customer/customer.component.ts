import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Customer } from './customer.model';
import { selectAllCustomers } from './customer.selectors';
import { State } from '../reducers';
import { createCustomer, deleteCustomers, loadCustomers } from './customer.actions';
import { v4 as uuid } from 'uuid';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { CustomerDeleteDialogComponent } from './customer-delete-dialog.component';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.sass'],
})
export class CustomerComponent implements OnInit {
  public dataSource: Observable<Customer[]>;
  public displayedColumns: string[] = [
    'id',
    'delete',
    'name',
  ];
  public selection = new SelectionModel<Customer>(true, []);

  constructor(private store: Store<State>, private dialog: MatDialog) {
    this.dataSource = this.store.select(selectAllCustomers);
  }

  ngOnInit(): void {
    this.store.dispatch(loadCustomers());
  }

  onDeleteClick(event: Event): void {
    // prevent parent event happening (navigation)
    event.preventDefault();
    event.stopImmediatePropagation();

    const dialogRef = this.dialog.open(CustomerDeleteDialogComponent, {
      data: {
        selection: this.selection.selected,
      },
    });
    dialogRef.afterClosed().subscribe((result: Customer[]) => {
      const ids = result.map((entity) => entity.id);
      if (ids && ids.length) {
        this.store.dispatch(deleteCustomers({ ids }));
        this.selection.clear();
      }
    });
  }

  toggleSelectAll(): void {
    if (this.selection.hasValue()) {
      this.selection.clear();
    } else {
      this.dataSource.forEach((rows) => rows.forEach((row) => this.selection.select(row)));
    }
  }

  create(entity: Partial<Customer>): void {
    const customer: Customer = {
      id: uuid(),
      name: entity.name!,
    };
    this.store.dispatch(createCustomer({ customer }));
  }
}

