import { reducer, initialState } from './customer.reducer';
import {
  createCustomer,
  updateCustomer,
  deleteCustomers,
  customerLoaded,
  customersLoaded
} from './customer.actions';

describe('Customer Reducer', () => {
  describe('unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;
      const result = reducer(initialState, action);
      expect(result).toBe(initialState);
    });
  });

  describe('updateCustomer', () => {
    it('should not update missing one', () => {
      const action = updateCustomer({ update: { id: '1', changes: {
        name: 'Malayan Tapir Updated',
      } } });
      const result = reducer(initialState, action);
      expect(result).toEqual({ ids: [], entities: {} });
    });

    it('should update one', () => {
      const customer = {
        id: '1',
        name: 'Malayan Tapir',
      };
      const action = updateCustomer({ update: { id: '1', changes: {
        name: 'Malayan Tapir Updated',
      } } });
      const result = reducer({ ids: ['1'], entities: { ['1']: customer } }, action);
      expect(result.ids).toEqual(['1']);
      expect(result.entities['1']).toEqual({
        id: '1',
        name: 'Malayan Tapir Updated',
      });
    });
  });

  describe('deleteCustomers', () => {
    it('should delete one', () => {
      const customer = {
        id: '1',
        name: 'Malayan Tapir',
      };
      const action = deleteCustomers({ ids: ['1'] });
      const result = reducer({ ids: ['1'], entities: { ['1']: customer } }, action);
      expect(result.ids).toEqual([]);
    });
  });

  describe('customerLoaded', () => {
    it('should set one', () => {
      const customer = {
        id: '1',
        name: 'Malayan Tapir',
      };
      const action = customerLoaded({ customer });
      const result = reducer(initialState, action);
      expect(result.ids).toEqual(['1']);
    });
  });

  describe('customersLoaded', () => {
    it('should sets all', () => {
      const customers = [
        {
          id: '1',
          name: 'Malayan Tapir',
        },
        {
          id: '2',
          name: 'Malayan Tapir',
        }
      ];
      const action = customersLoaded({ customers });
      const result = reducer(initialState, action);
      expect(result.ids).toEqual(['1', '2']);
    });
  });
});
