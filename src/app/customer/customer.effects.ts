import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { CustomerService } from './customer.service';
import {
  createCustomer, customerLoaded, customersLoaded,
  deleteCustomers, loadCustomer, loadCustomers, updateCustomer
} from './customer.actions';
import { concatMap, map } from 'rxjs/operators';

@Injectable()
export class CustomerEffects {
  constructor(private service: CustomerService, private actions$: Actions) { }

  loadCustomers$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadCustomers),
      concatMap(() => this.service.getAll()),
      map((customers) => customersLoaded({ customers }))
    )
  );

  loadCustomer$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadCustomer),
      concatMap((action) => this.service.getOne(action.id)),
      map((customer) => customerLoaded({ customer }))
    )
  );

  createCustomer$ = createEffect(() =>
    this.actions$.pipe(
      ofType(createCustomer),
      concatMap((action) => this.service.create(action.customer)),
      map((customer) => loadCustomers())
    )
  );

  updateCustomer$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateCustomer),
      concatMap((action) => this.service.update(action.update)),
      map((customer) => loadCustomers())
    )
  );

  deleteCustomers$ = createEffect(() =>
    this.actions$.pipe(
      ofType(deleteCustomers),
      concatMap((action) => this.service.delete(action.ids)),
      map((customer) => loadCustomers())
    )
  );
}
