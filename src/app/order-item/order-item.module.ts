import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderItemComponent } from './order-item.component';
import { OrderItemUpdateComponent } from './order-item-update.component';
import { OrderItemDeleteDialogComponent } from './order-item-delete-dialog.component';
import { StoreModule } from '@ngrx/store';
import * as fromOrderItem from './order-item.reducer';
import { SharedModule } from '../shared/shared.module';
import { OrderItemFormComponent } from './order-item-form.component';
import { EffectsModule } from '@ngrx/effects';
import { OrderItemEffects } from './order-item.effects';

@NgModule({
  declarations: [
    OrderItemComponent,
    OrderItemFormComponent,
    OrderItemUpdateComponent,
    OrderItemDeleteDialogComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    StoreModule.forFeature(fromOrderItem.orderItemsFeatureKey, fromOrderItem.reducer),
    EffectsModule.forFeature([OrderItemEffects])
  ]
})
export class OrderItemModule { }
