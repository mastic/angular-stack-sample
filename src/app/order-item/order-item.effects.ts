import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { OrderItemService } from './order-item.service';
import {
  createOrderItem, orderItemLoaded, orderItemsLoaded,
  deleteOrderItems, loadOrderItem, loadOrderItems, updateOrderItem
} from './order-item.actions';
import { concatMap, map } from 'rxjs/operators';

@Injectable()
export class OrderItemEffects {
  constructor(private service: OrderItemService, private actions$: Actions) { }

  loadOrderItems$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadOrderItems),
      concatMap(() => this.service.getAll()),
      map((orderItems) => orderItemsLoaded({ orderItems }))
    )
  );

  loadOrderItem$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadOrderItem),
      concatMap((action) => this.service.getOne(action.id)),
      map((orderItem) => orderItemLoaded({ orderItem }))
    )
  );

  createOrderItem$ = createEffect(() =>
    this.actions$.pipe(
      ofType(createOrderItem),
      concatMap((action) => this.service.create(action.orderItem)),
      map((orderItem) => loadOrderItems())
    )
  );

  updateOrderItem$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateOrderItem),
      concatMap((action) => this.service.update(action.update)),
      map((orderItem) => loadOrderItems())
    )
  );

  deleteOrderItems$ = createEffect(() =>
    this.actions$.pipe(
      ofType(deleteOrderItems),
      concatMap((action) => this.service.delete(action.ids)),
      map((orderItem) => loadOrderItems())
    )
  );
}
