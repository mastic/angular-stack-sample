import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { OrderItem } from './order-item.model';
import { Store } from '@ngrx/store';
import { State } from '../reducers';
import { selectCurrentOrderItem } from './order-item.selectors';
import { loadOrderItem } from './order-item.actions';
import { notNull } from '../reducers/common';

@Component({
  selector: 'app-order-item-update',
  templateUrl: './order-item-update.component.html',
  styleUrls: ['./order-item-update.component.sass'],
})
export class OrderItemUpdateComponent implements OnInit {
  orderItem$: Observable<OrderItem>;

  constructor(private store: Store<State>, private route: ActivatedRoute) {
    this.route.params.pipe(map((params) => params['key'])).subscribe((id) => {
      this.store.dispatch(loadOrderItem({ id }));
    });
    this.orderItem$ = this.store.select(selectCurrentOrderItem)
      .pipe(notNull());
  }

  ngOnInit(): void {}
}

