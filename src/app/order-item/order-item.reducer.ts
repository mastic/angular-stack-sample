import { Action, createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { OrderItem } from './order-item.model';
import * as OrderItemActions from './order-item.actions';

export const orderItemsFeatureKey = 'orderItems';

export interface State extends EntityState<OrderItem> {
  entity?: OrderItem;
}

export const adapter: EntityAdapter<OrderItem> = createEntityAdapter<OrderItem>();

export const initialState: State = adapter.getInitialState({
  // additional entity state properties
});

export const reducer = createReducer(
  initialState,
  on(OrderItemActions.updateOrderItem,
    (state, action) => adapter.updateOne(action.update, state)
  ),
  on(OrderItemActions.deleteOrderItems,
    (state, action) => adapter.removeMany(action.ids, state)
  ),
  on(OrderItemActions.orderItemLoaded,
    (state, action) => {
      return { ...adapter.setOne(action.orderItem, state), entity: action.orderItem }
    }
  ),
  on(OrderItemActions.orderItemsLoaded,
    (state, action) => adapter.setAll(action.orderItems, state)
  ),
);

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();
