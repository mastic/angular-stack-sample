import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { OrderItem } from './order-item.model';

@Component({
  selector: 'app-order-item-delete-dialog',
  templateUrl: './order-item-delete-dialog.component.html',
  styleUrls: ['./order-item-delete-dialog.component.sass'],
})
export class OrderItemDeleteDialogComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {selection: OrderItem[]},
    public dialogRef: MatDialogRef<OrderItemDeleteDialogComponent>
  ) {}

  ngOnInit(): void {}
}
