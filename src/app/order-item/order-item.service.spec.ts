import { TestBed } from '@angular/core/testing';

import { OrderItemService } from './order-item.service';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { SharedModule } from '../shared/shared.module';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('OrderItemService', () => {
  let service: OrderItemService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers), HttpClientTestingModule],
    });
    service = TestBed.inject(OrderItemService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  const orderItems = [
        {
          id: '1',
          amount: 17983,
        },
        {
          id: '2',
          amount: 17983,
        }
  ];

  describe('#getAll', () => {
    it('should return an Observable<OrderItem[]>', () => {
      service.getAll().subscribe((result) => {
        expect(result.length).toBe(2);
        expect(result).toEqual(result);
      });

      const req = httpMock.expectOne(`/api/order-items`);
      expect(req.request.method).toBe('GET');
      req.flush(orderItems);
    });
  });

  describe('#getOne', () => {
    it('should return an Observable<OrderItem>', () => {
      service.getOne('42').subscribe((result) => {
        expect(result).toBeTruthy();
        expect(result).toEqual(result);
      });

      const req = httpMock.expectOne(`/api/order-items/42`);
      expect(req.request.method).toBe('GET');
      req.flush(orderItems);
    });
  });

  describe('#create', () => {
    it('should return an Observable<OrderItem>', () => {
      service.create(orderItems[0]).subscribe((result) => {
        expect(result).toBeTruthy();
        expect(result).toEqual(result);
      });

      const req = httpMock.expectOne(`/api/order-items`);
      expect(req.request.method).toBe('POST');
      req.flush(orderItems);
    });
  });

  describe('#delete', () => {
    it('should return an Observable<OrderItem>', () => {
      service.delete(['42']).subscribe((result) => {
        expect(result).toBeTruthy();
        expect(result).toEqual(result);
      });

      const req = httpMock.expectOne(`/api/order-items/42`);
      expect(req.request.method).toBe('DELETE');
      req.flush(orderItems);
    });
  });

  describe('#update', () => {
    it('should return an Observable<OrderItem>', () => {
      service.update({id: '42', changes: {}}).subscribe((result) => {
        expect(result).toBeTruthy();
        expect(result).toEqual(result);
      });

      const req = httpMock.expectOne(`/api/order-items/42`);
      expect(req.request.method).toBe('PUT');
      req.flush(orderItems);
    });
  });

});
