// mastic-pin-import-statements

export interface OrderItem {
  id: string;
  amount: number;
}
