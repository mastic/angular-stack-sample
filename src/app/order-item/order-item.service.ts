import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, forkJoin } from 'rxjs';
import { OrderItem } from './order-item.model';
import { Update } from '@ngrx/entity';

@Injectable({
  providedIn: 'root',
})
export class OrderItemService {
  http: HttpClient;

  constructor(http: HttpClient) {
    this.http = http;
  }

  getAll(): Observable<OrderItem[]> {
    return this.http.get<OrderItem[]>(`/api/order-items`);
  }

  getOne(key: string): Observable<OrderItem> {
    return this.http.get<OrderItem>(`/api/order-items/${key}`);
  }

  create(orderItem: OrderItem): Observable<OrderItem> {
    return this.http.post<OrderItem>(`/api/order-items`, orderItem);
  }

  delete(keys: string[]): Observable<any> {
    return forkJoin(keys.map(key => this.http.delete(`/api/order-items/${key}`)));
  }

  update(update: Update<OrderItem>): Observable<any> {
    return this.http.put(`/api/order-items/${update.id}`, update.changes);
  }
}
