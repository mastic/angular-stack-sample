import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OrderItemUpdateComponent } from './order-item-update.component';
import { SharedModule } from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { RouterModule } from '@angular/router';
import { OrderItemFormComponent } from './order-item-form.component';

describe('OrderItemUpdateComponent', () => {
  let component: OrderItemUpdateComponent;
  let fixture: ComponentFixture<OrderItemUpdateComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers), RouterModule.forRoot([], { relativeLinkResolution: 'legacy' })],
      declarations: [OrderItemUpdateComponent, OrderItemFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderItemUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
