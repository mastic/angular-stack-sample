import { reducer, initialState } from './order-item.reducer';
import {
  createOrderItem,
  updateOrderItem,
  deleteOrderItems,
  orderItemLoaded,
  orderItemsLoaded
} from './order-item.actions';

describe('OrderItem Reducer', () => {
  describe('unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;
      const result = reducer(initialState, action);
      expect(result).toBe(initialState);
    });
  });

  describe('updateOrderItem', () => {
    it('should not update missing one', () => {
      const action = updateOrderItem({ update: { id: '1', changes: {
        amount: 18025,
      } } });
      const result = reducer(initialState, action);
      expect(result).toEqual({ ids: [], entities: {} });
    });

    it('should update one', () => {
      const orderItem = {
        id: '1',
        amount: 17983,
      };
      const action = updateOrderItem({ update: { id: '1', changes: {
        amount: 18025,
      } } });
      const result = reducer({ ids: ['1'], entities: { ['1']: orderItem } }, action);
      expect(result.ids).toEqual(['1']);
      expect(result.entities['1']).toEqual({
        id: '1',
        amount: 18025,
      });
    });
  });

  describe('deleteOrderItems', () => {
    it('should delete one', () => {
      const orderItem = {
        id: '1',
        amount: 17983,
      };
      const action = deleteOrderItems({ ids: ['1'] });
      const result = reducer({ ids: ['1'], entities: { ['1']: orderItem } }, action);
      expect(result.ids).toEqual([]);
    });
  });

  describe('orderItemLoaded', () => {
    it('should set one', () => {
      const orderItem = {
        id: '1',
        amount: 17983,
      };
      const action = orderItemLoaded({ orderItem });
      const result = reducer(initialState, action);
      expect(result.ids).toEqual(['1']);
    });
  });

  describe('orderItemsLoaded', () => {
    it('should sets all', () => {
      const orderItems = [
        {
          id: '1',
          amount: 17983,
        },
        {
          id: '2',
          amount: 17983,
        }
      ];
      const action = orderItemsLoaded({ orderItems });
      const result = reducer(initialState, action);
      expect(result.ids).toEqual(['1', '2']);
    });
  });
});
