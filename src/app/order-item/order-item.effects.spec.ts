import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable, throwError, of } from 'rxjs';
import { cold } from 'jasmine-marbles';

import { OrderItemEffects } from './order-item.effects';
import {
  loadOrderItem,
  loadOrderItems,
  orderItemLoaded,
  orderItemsLoaded,
  createOrderItem,
  updateOrderItem,
  deleteOrderItems,
} from './order-item.actions';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { SharedModule } from '../shared/shared.module';
import { Actions } from '@ngrx/effects';

const serviceStub = (method: string, response: any) => {
  const service = jasmine.createSpyObj('orderItem', [method]);
  const isError = response instanceof Error;
  const serviceResponse = isError ? throwError(response) : of(response);
  service[method].and.returnValue(serviceResponse);
  return service;
};

describe('OrderItemEffects', () => {
  // tslint:disable-next-line: prefer-const
  let actions$: Observable<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers)],
      providers: [OrderItemEffects, provideMockActions(() => actions$)],
    });
  });

  it('should be created', () => {
    const effects = TestBed.inject(OrderItemEffects);
    expect(effects).toBeTruthy();
  });

  it('loadOrderItems should return orderItemsLoaded', () => {
    const source = cold('a', { a: loadOrderItems() });
    const service = serviceStub('getAll', []);
    const effects = new OrderItemEffects(service, new Actions(source));

    const expected = cold('a', { a: orderItemsLoaded({ orderItems: [] }) });
    expect(effects.loadOrderItems$).toBeObservable(expected);
  });

  it('loadOrderItem should return orderItemLoaded', () => {
    const source = cold('a', { a: loadOrderItem({ id: '42' }) });
    const orderItem = {
      id: '1',
      amount: 17983,
    };
    const service = serviceStub('getOne', orderItem);
    const effects = new OrderItemEffects(service, new Actions(source));

    const expected = cold('a', { a: orderItemLoaded({ orderItem }) });
    expect(effects.loadOrderItem$).toBeObservable(expected);
  });

  it('createOrderItem should return orderItemLoaded', () => {
    const orderItem = {
      id: '1',
      amount: 17983,
    };
    const source = cold('a', { a: createOrderItem({ orderItem }) });
    const service = serviceStub('create', orderItem);
    const effects = new OrderItemEffects(service, new Actions(source));

    const expected = cold('a', { a: loadOrderItems() });
    expect(effects.createOrderItem$).toBeObservable(expected);
  });

  it('updateOrderItem should return loadOrderItems', () => {
    const source = cold('a', { a: updateOrderItem({ update: { id: '1', changes: {} } }) });
    const orderItem = {
      id: '1',
      amount: 17983,
    };
    const service = serviceStub('update', orderItem);
    const effects = new OrderItemEffects(service, new Actions(source));

    const expected = cold('a', { a: loadOrderItems() });
    expect(effects.updateOrderItem$).toBeObservable(expected);
  });

  it('deleteOrderItems should return loadOrderItems', () => {
    const source = cold('a', { a: deleteOrderItems({ ids: ['1'] }) });
    const orderItem = {
      id: '1',
      amount: 17983,
    };
    const service = serviceStub('delete', orderItem);
    const effects = new OrderItemEffects(service, new Actions(source));

    const expected = cold('a', { a: loadOrderItems() });
    expect(effects.deleteOrderItems$).toBeObservable(expected);
  });
});
