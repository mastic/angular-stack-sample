import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { State } from '../reducers';
import { createOrderItem, updateOrderItem } from './order-item.actions';
import { Observable } from 'rxjs';
import { OrderItem } from './order-item.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-order-item-form',
  templateUrl: './order-item-form.component.html',
  styleUrls: ['./order-item-form.component.sass'],
})
export class OrderItemFormComponent implements OnInit {
  @Input() orderItem!: Observable<OrderItem>;

  orderItemForm = new FormGroup({
    id: new FormControl(''),
    amount: new FormControl(''),
  });

  constructor(private store: Store<State>, private router: Router) {}

  ngOnInit(): void {
    if (this.orderItem) {
      this.orderItem.subscribe((orderItem) => {
        if (orderItem && orderItem.id) {
          this.orderItemForm.setValue(orderItem);
        }
      });
    }
  }

  get amount(): FormControl {
    return this.orderItemForm.get('amount') as FormControl;
  }

  onSubmit(): void {
    if (this.orderItemForm.valid) {
      if (this.orderItemForm.value.id) {
        this.store.dispatch(
          updateOrderItem({
            update: {
              id: this.orderItemForm.value.id,
              changes: this.orderItemForm.value,
            },
          })
        );
        this.router.navigate(['/order-item']);
      } else {
        const value = this.orderItemForm.value;
        const { id, ...orderItem } = value;
        this.store.dispatch(createOrderItem({ orderItem }));
      }
    }
  }
}
