import { createAction, props } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { OrderItem } from './order-item.model';

export const loadOrderItems = createAction('[OrderItem/API] Load Order Items');
export const orderItemsLoaded = createAction('[OrderItem/API] Order Items Loaded Successfully',
  props<{ orderItems: OrderItem[] }>());

export const loadOrderItem = createAction('[OrderItem/API] Load OrderItem', props<{ id: string }>());
export const orderItemLoaded = createAction('[OrderItem/API] OrderItem Loaded Successfully',
  props<{ orderItem: OrderItem }>());

export const createOrderItem = createAction('[OrderItem/API] Create OrderItem',
  props<{ orderItem: OrderItem }>());

export const deleteOrderItems = createAction('[OrderItem/API] Delete Order Items',
  props<{ ids: string[] }>());

export const updateOrderItem = createAction('[OrderItem/API] Update OrderItem',
  props<{ update: Update<OrderItem> }>());

export const orderItemActionTypes = {
  loadOrderItems,
  orderItemsLoaded,
  loadOrderItem,
  orderItemLoaded,
  createOrderItem,
  deleteOrderItems,
  updateOrderItem,
};
