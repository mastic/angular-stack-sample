import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OrderItemFormComponent } from './order-item-form.component';
import { SharedModule } from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { RouterModule } from '@angular/router';

const createNewEvent = (eventName: string, bubbles = false, cancelable = false): CustomEvent => {
  const evt = document.createEvent('CustomEvent');
  evt.initCustomEvent(eventName, bubbles, cancelable, null);
  return evt;
};

describe('OrderItemFormComponent', () => {
  let component: OrderItemFormComponent;
  let fixture: ComponentFixture<OrderItemFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers), RouterModule.forRoot([], { relativeLinkResolution: 'legacy' })],
      declarations: [OrderItemFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderItemFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have amount', () => {
    expect(component.amount).toBeTruthy();
  });

  it('initially should be invalid', () => {
    expect(component.orderItemForm.valid).toBeFalsy();
  });

  it('initially should be valid upon input', () => {
    const event = createNewEvent('input');

    const amount = fixture.nativeElement.querySelector('#amount');
    amount.value = 17983;
    amount.dispatchEvent(event);

    expect(component.orderItemForm.valid).toBeTruthy();
  });
});
