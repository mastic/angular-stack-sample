import { createFeatureSelector, createSelector } from '@ngrx/store';
import { orderItemsFeatureKey, selectAll, State } from './order-item.reducer';

const orderItemFeatureSelector = createFeatureSelector<State>(orderItemsFeatureKey);
export const selectAllOrderItems = createSelector(orderItemFeatureSelector, selectAll);
export const selectCurrentOrderItem = createSelector(orderItemFeatureSelector,
    s => s.entity,
);
