import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { OrderItem } from './order-item.model';
import { selectAllOrderItems } from './order-item.selectors';
import { State } from '../reducers';
import { createOrderItem, deleteOrderItems, loadOrderItems } from './order-item.actions';
import { v4 as uuid } from 'uuid';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { OrderItemDeleteDialogComponent } from './order-item-delete-dialog.component';

@Component({
  selector: 'app-order-item',
  templateUrl: './order-item.component.html',
  styleUrls: ['./order-item.component.sass'],
})
export class OrderItemComponent implements OnInit {
  public dataSource: Observable<OrderItem[]>;
  public displayedColumns: string[] = [
    'id',
    'delete',
    'amount',
  ];
  public selection = new SelectionModel<OrderItem>(true, []);

  constructor(private store: Store<State>, private dialog: MatDialog) {
    this.dataSource = this.store.select(selectAllOrderItems);
  }

  ngOnInit(): void {
    this.store.dispatch(loadOrderItems());
  }

  onDeleteClick(event: Event): void {
    // prevent parent event happening (navigation)
    event.preventDefault();
    event.stopImmediatePropagation();

    const dialogRef = this.dialog.open(OrderItemDeleteDialogComponent, {
      data: {
        selection: this.selection.selected,
      },
    });
    dialogRef.afterClosed().subscribe((result: OrderItem[]) => {
      const ids = result.map((entity) => entity.id);
      if (ids && ids.length) {
        this.store.dispatch(deleteOrderItems({ ids }));
        this.selection.clear();
      }
    });
  }

  toggleSelectAll(): void {
    if (this.selection.hasValue()) {
      this.selection.clear();
    } else {
      this.dataSource.forEach((rows) => rows.forEach((row) => this.selection.select(row)));
    }
  }

  create(entity: Partial<OrderItem>): void {
    const orderItem: OrderItem = {
      id: uuid(),
      amount: entity.amount!,
    };
    this.store.dispatch(createOrderItem({ orderItem }));
  }
}

