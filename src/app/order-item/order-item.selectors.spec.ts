import { State } from './order-item.reducer';
import { selectAllOrderItems } from './order-item.selectors';

const state: State = {
  ids: ['1', '2', '3'],
  entities: {
    1: { id: '1',
           amount: 17983,
      },
      2: { id: '2',
           amount: 17983,
      },
      3: { id: '3',
           amount: 17983,
      },
  },
};

describe('OrderItem Selectors', () => {
  it('selectAllOrderItems', () => {
    expect(selectAllOrderItems.projector(state)).toEqual([
      {
        id: '1',
        amount: 17983,
      },
      {
        id: '2',
        amount: 17983,
      },
      {
        id: '3',
        amount: 17983,
      }
    ]);
  });
});
