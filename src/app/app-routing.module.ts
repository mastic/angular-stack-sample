import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { PageNotFoundComponent } from './error/page-not-found.component';
import { CustomerComponent } from './customer/customer.component';
import { CustomerUpdateComponent } from './customer/customer-update.component';
import { OrderComponent } from './order/order.component';
import { OrderUpdateComponent } from './order/order-update.component';
import { ProductComponent } from './product/product.component';
import { ProductUpdateComponent } from './product/product-update.component';
import { OrderItemComponent } from './order-item/order-item.component';
import { OrderItemUpdateComponent } from './order-item/order-item-update.component';
// mastic-pin-import-statements

const routes: Routes = [
  { path: 'welcome', component: WelcomeComponent },
  { path: '', redirectTo: '/welcome', pathMatch: 'full' },
  { path: 'customer', component: CustomerComponent },
  { path: 'customer/:id', component: CustomerUpdateComponent },
  { path: 'order', component: OrderComponent },
  { path: 'order/:id', component: OrderUpdateComponent },
  { path: 'product', component: ProductComponent },
  { path: 'product/:id', component: ProductUpdateComponent },
  { path: 'order-item', component: OrderItemComponent },
  { path: 'order-item/:id', component: OrderItemUpdateComponent },
  // mastic-pin-routing-mid-route
  { path: '**', component: PageNotFoundComponent },
  // mastic-pin-routing-last-route
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
