import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';

import { WelcomeComponent } from './welcome/welcome.component';
import { HeaderComponent } from './header/header.component';
import { SideNavigationComponent } from './side-navigation/side-navigation.component';
import { PageNotFoundComponent } from './error/page-not-found.component';
import { ElevateDirective } from './elevate.directive';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { reducers, metaReducers } from './reducers';
import { environment } from '../environments/environment';
import { CustomerEffects } from './customer/customer.effects';
import { CustomerModule } from './customer/customer.module';
import { OrderEffects } from './order/order.effects';
import { OrderModule } from './order/order.module';
import { ProductEffects } from './product/product.effects';
import { ProductModule } from './product/product.module';
import { OrderItemEffects } from './order-item/order-item.effects';
import { OrderItemModule } from './order-item/order-item.module';
// mastic-pin-app-import-statements

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    HeaderComponent,
    SideNavigationComponent,
    PageNotFoundComponent,
    ElevateDirective,
    // mastic-pin-app-declarations
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    CustomerModule,
    EffectsModule.forRoot([
      CustomerEffects,
      OrderEffects,
      ProductEffects,
      OrderItemEffects,
    ]),
    StoreModule.forRoot(reducers, { metaReducers }),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
    OrderModule,
    ProductModule,
    OrderItemModule,
    // mastic-pin-app-imports
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
