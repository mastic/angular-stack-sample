import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { State } from '../reducers';
import { createOrder, updateOrder } from './order.actions';
import { Observable } from 'rxjs';
import { Order } from './order.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-order-form',
  templateUrl: './order-form.component.html',
  styleUrls: ['./order-form.component.sass'],
})
export class OrderFormComponent implements OnInit {
  @Input() order!: Observable<Order>;

  orderForm = new FormGroup({
    id: new FormControl(''),
    orderDate: new FormControl(''),
    deliveryDate: new FormControl(''),
    notes: new FormControl(''),
  });

  constructor(private store: Store<State>, private router: Router) {}

  ngOnInit(): void {
    if (this.order) {
      this.order.subscribe((order) => {
        if (order && order.id) {
          this.orderForm.setValue(order);
        }
      });
    }
  }

  get orderDate(): FormControl {
    return this.orderForm.get('orderDate') as FormControl;
  }

  get deliveryDate(): FormControl {
    return this.orderForm.get('deliveryDate') as FormControl;
  }

  get notes(): FormControl {
    return this.orderForm.get('notes') as FormControl;
  }

  onSubmit(): void {
    if (this.orderForm.valid) {
      if (this.orderForm.value.id) {
        this.store.dispatch(
          updateOrder({
            update: {
              id: this.orderForm.value.id,
              changes: this.orderForm.value,
            },
          })
        );
        this.router.navigate(['/order']);
      } else {
        const value = this.orderForm.value;
        const { id, ...order } = value;
        this.store.dispatch(createOrder({ order }));
      }
    }
  }
}
