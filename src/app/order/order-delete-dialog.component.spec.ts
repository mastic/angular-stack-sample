import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OrderDeleteDialogComponent } from './order-delete-dialog.component';
import { SharedModule } from '../shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

describe('OrderDeleteDialogComponent', () => {
  let component: OrderDeleteDialogComponent;
  let fixture: ComponentFixture<OrderDeleteDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [OrderDeleteDialogComponent],
      imports: [SharedModule, BrowserAnimationsModule],
      providers: [
        { provide: MatDialogRef, useValue: {} },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            selection: [{
              id: '1',
              orderDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
              deliveryDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
              notes: 'Malayan Tapir',
            }],
          },
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderDeleteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have data', () => {
    expect(component.data).toBeTruthy();
    expect(component.data.selection).toBeTruthy();
    expect(component.data.selection.length).toEqual(1);
  });

});
