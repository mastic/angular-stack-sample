import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable, throwError, of } from 'rxjs';
import { cold } from 'jasmine-marbles';

import { OrderEffects } from './order.effects';
import {
  loadOrder,
  loadOrders,
  orderLoaded,
  ordersLoaded,
  createOrder,
  updateOrder,
  deleteOrders,
} from './order.actions';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { SharedModule } from '../shared/shared.module';
import { Actions } from '@ngrx/effects';

const serviceStub = (method: string, response: any) => {
  const service = jasmine.createSpyObj('order', [method]);
  const isError = response instanceof Error;
  const serviceResponse = isError ? throwError(response) : of(response);
  service[method].and.returnValue(serviceResponse);
  return service;
};

describe('OrderEffects', () => {
  // tslint:disable-next-line: prefer-const
  let actions$: Observable<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers)],
      providers: [OrderEffects, provideMockActions(() => actions$)],
    });
  });

  it('should be created', () => {
    const effects = TestBed.inject(OrderEffects);
    expect(effects).toBeTruthy();
  });

  it('loadOrders should return ordersLoaded', () => {
    const source = cold('a', { a: loadOrders() });
    const service = serviceStub('getAll', []);
    const effects = new OrderEffects(service, new Actions(source));

    const expected = cold('a', { a: ordersLoaded({ orders: [] }) });
    expect(effects.loadOrders$).toBeObservable(expected);
  });

  it('loadOrder should return orderLoaded', () => {
    const source = cold('a', { a: loadOrder({ id: '42' }) });
    const order = {
      id: '1',
      orderDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
      deliveryDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
      notes: 'Malayan Tapir',
    };
    const service = serviceStub('getOne', order);
    const effects = new OrderEffects(service, new Actions(source));

    const expected = cold('a', { a: orderLoaded({ order }) });
    expect(effects.loadOrder$).toBeObservable(expected);
  });

  it('createOrder should return orderLoaded', () => {
    const order = {
      id: '1',
      orderDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
      deliveryDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
      notes: 'Malayan Tapir',
    };
    const source = cold('a', { a: createOrder({ order }) });
    const service = serviceStub('create', order);
    const effects = new OrderEffects(service, new Actions(source));

    const expected = cold('a', { a: loadOrders() });
    expect(effects.createOrder$).toBeObservable(expected);
  });

  it('updateOrder should return loadOrders', () => {
    const source = cold('a', { a: updateOrder({ update: { id: '1', changes: {} } }) });
    const order = {
      id: '1',
      orderDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
      deliveryDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
      notes: 'Malayan Tapir',
    };
    const service = serviceStub('update', order);
    const effects = new OrderEffects(service, new Actions(source));

    const expected = cold('a', { a: loadOrders() });
    expect(effects.updateOrder$).toBeObservable(expected);
  });

  it('deleteOrders should return loadOrders', () => {
    const source = cold('a', { a: deleteOrders({ ids: ['1'] }) });
    const order = {
      id: '1',
      orderDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
      deliveryDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
      notes: 'Malayan Tapir',
    };
    const service = serviceStub('delete', order);
    const effects = new OrderEffects(service, new Actions(source));

    const expected = cold('a', { a: loadOrders() });
    expect(effects.deleteOrders$).toBeObservable(expected);
  });
});
