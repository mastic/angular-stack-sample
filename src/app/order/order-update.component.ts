import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Order } from './order.model';
import { Store } from '@ngrx/store';
import { State } from '../reducers';
import { selectCurrentOrder } from './order.selectors';
import { loadOrder } from './order.actions';
import { notNull } from '../reducers/common';

@Component({
  selector: 'app-order-update',
  templateUrl: './order-update.component.html',
  styleUrls: ['./order-update.component.sass'],
})
export class OrderUpdateComponent implements OnInit {
  order$: Observable<Order>;

  constructor(private store: Store<State>, private route: ActivatedRoute) {
    this.route.params.pipe(map((params) => params['key'])).subscribe((id) => {
      this.store.dispatch(loadOrder({ id }));
    });
    this.order$ = this.store.select(selectCurrentOrder)
      .pipe(notNull());
  }

  ngOnInit(): void {}
}

