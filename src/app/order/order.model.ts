// mastic-pin-import-statements

export interface Order {
  id: string;
  orderDate: Date;
  deliveryDate: Date;
  notes?: string;
}
