import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OrderFormComponent } from './order-form.component';
import { SharedModule } from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { RouterModule } from '@angular/router';

const createNewEvent = (eventName: string, bubbles = false, cancelable = false): CustomEvent => {
  const evt = document.createEvent('CustomEvent');
  evt.initCustomEvent(eventName, bubbles, cancelable, null);
  return evt;
};

describe('OrderFormComponent', () => {
  let component: OrderFormComponent;
  let fixture: ComponentFixture<OrderFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers), RouterModule.forRoot([], { relativeLinkResolution: 'legacy' })],
      declarations: [OrderFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have orderDate', () => {
    expect(component.orderDate).toBeTruthy();
  });

  it('should have deliveryDate', () => {
    expect(component.deliveryDate).toBeTruthy();
  });

  it('should have notes', () => {
    expect(component.notes).toBeTruthy();
  });

  it('initially should be invalid', () => {
    expect(component.orderForm.valid).toBeFalsy();
  });

  it('initially should be valid upon input', () => {
    const event = createNewEvent('input');

    const orderDate = fixture.nativeElement.querySelector('#orderDate');
    orderDate.value = new Date(Date.UTC(2020, 1, 0, 9, 0)).toISOString().split('T')[0];
    orderDate.dispatchEvent(event);

    const deliveryDate = fixture.nativeElement.querySelector('#deliveryDate');
    deliveryDate.value = new Date(Date.UTC(2020, 1, 0, 9, 0)).toISOString().split('T')[0];
    deliveryDate.dispatchEvent(event);

    const notes = fixture.nativeElement.querySelector('#notes');
    notes.value = 'Malayan Tapir';
    notes.dispatchEvent(event);

    expect(component.orderForm.valid).toBeTruthy();
  });
});
