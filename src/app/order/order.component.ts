import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Order } from './order.model';
import { selectAllOrders } from './order.selectors';
import { State } from '../reducers';
import { createOrder, deleteOrders, loadOrders } from './order.actions';
import { v4 as uuid } from 'uuid';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { OrderDeleteDialogComponent } from './order-delete-dialog.component';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.sass'],
})
export class OrderComponent implements OnInit {
  public dataSource: Observable<Order[]>;
  public displayedColumns: string[] = [
    'id',
    'delete',
    'orderDate',
    'deliveryDate',
    'notes',
  ];
  public selection = new SelectionModel<Order>(true, []);

  constructor(private store: Store<State>, private dialog: MatDialog) {
    this.dataSource = this.store.select(selectAllOrders);
  }

  ngOnInit(): void {
    this.store.dispatch(loadOrders());
  }

  onDeleteClick(event: Event): void {
    // prevent parent event happening (navigation)
    event.preventDefault();
    event.stopImmediatePropagation();

    const dialogRef = this.dialog.open(OrderDeleteDialogComponent, {
      data: {
        selection: this.selection.selected,
      },
    });
    dialogRef.afterClosed().subscribe((result: Order[]) => {
      const ids = result.map((entity) => entity.id);
      if (ids && ids.length) {
        this.store.dispatch(deleteOrders({ ids }));
        this.selection.clear();
      }
    });
  }

  toggleSelectAll(): void {
    if (this.selection.hasValue()) {
      this.selection.clear();
    } else {
      this.dataSource.forEach((rows) => rows.forEach((row) => this.selection.select(row)));
    }
  }

  create(entity: Partial<Order>): void {
    const order: Order = {
      id: uuid(),
      orderDate: entity.orderDate!,
      deliveryDate: entity.deliveryDate!,
      notes: entity.notes!,
    };
    this.store.dispatch(createOrder({ order }));
  }
}

