import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ordersFeatureKey, selectAll, State } from './order.reducer';

const orderFeatureSelector = createFeatureSelector<State>(ordersFeatureKey);
export const selectAllOrders = createSelector(orderFeatureSelector, selectAll);
export const selectCurrentOrder = createSelector(orderFeatureSelector,
    s => s.entity,
);
