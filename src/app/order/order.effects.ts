import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { OrderService } from './order.service';
import {
  createOrder, orderLoaded, ordersLoaded,
  deleteOrders, loadOrder, loadOrders, updateOrder
} from './order.actions';
import { concatMap, map } from 'rxjs/operators';

@Injectable()
export class OrderEffects {
  constructor(private service: OrderService, private actions$: Actions) { }

  loadOrders$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadOrders),
      concatMap(() => this.service.getAll()),
      map((orders) => ordersLoaded({ orders }))
    )
  );

  loadOrder$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadOrder),
      concatMap((action) => this.service.getOne(action.id)),
      map((order) => orderLoaded({ order }))
    )
  );

  createOrder$ = createEffect(() =>
    this.actions$.pipe(
      ofType(createOrder),
      concatMap((action) => this.service.create(action.order)),
      map((order) => loadOrders())
    )
  );

  updateOrder$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateOrder),
      concatMap((action) => this.service.update(action.update)),
      map((order) => loadOrders())
    )
  );

  deleteOrders$ = createEffect(() =>
    this.actions$.pipe(
      ofType(deleteOrders),
      concatMap((action) => this.service.delete(action.ids)),
      map((order) => loadOrders())
    )
  );
}
