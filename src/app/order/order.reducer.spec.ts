import { reducer, initialState } from './order.reducer';
import {
  createOrder,
  updateOrder,
  deleteOrders,
  orderLoaded,
  ordersLoaded
} from './order.actions';

describe('Order Reducer', () => {
  describe('unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;
      const result = reducer(initialState, action);
      expect(result).toBe(initialState);
    });
  });

  describe('updateOrder', () => {
    it('should not update missing one', () => {
      const action = updateOrder({ update: { id: '1', changes: {
        orderDate: new Date(Date.UTC(2020, 1, 1, 9, 0)),
        deliveryDate: new Date(Date.UTC(2020, 1, 1, 9, 0)),
        notes: 'Malayan Tapir Updated',
      } } });
      const result = reducer(initialState, action);
      expect(result).toEqual({ ids: [], entities: {} });
    });

    it('should update one', () => {
      const order = {
        id: '1',
        orderDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
        deliveryDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
        notes: 'Malayan Tapir',
      };
      const action = updateOrder({ update: { id: '1', changes: {
        orderDate: new Date(Date.UTC(2020, 1, 1, 9, 0)),
        deliveryDate: new Date(Date.UTC(2020, 1, 1, 9, 0)),
        notes: 'Malayan Tapir Updated',
      } } });
      const result = reducer({ ids: ['1'], entities: { ['1']: order } }, action);
      expect(result.ids).toEqual(['1']);
      expect(result.entities['1']).toEqual({
        id: '1',
        orderDate: new Date(Date.UTC(2020, 1, 1, 9, 0)),
        deliveryDate: new Date(Date.UTC(2020, 1, 1, 9, 0)),
        notes: 'Malayan Tapir Updated',
      });
    });
  });

  describe('deleteOrders', () => {
    it('should delete one', () => {
      const order = {
        id: '1',
        orderDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
        deliveryDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
        notes: 'Malayan Tapir',
      };
      const action = deleteOrders({ ids: ['1'] });
      const result = reducer({ ids: ['1'], entities: { ['1']: order } }, action);
      expect(result.ids).toEqual([]);
    });
  });

  describe('orderLoaded', () => {
    it('should set one', () => {
      const order = {
        id: '1',
        orderDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
        deliveryDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
        notes: 'Malayan Tapir',
      };
      const action = orderLoaded({ order });
      const result = reducer(initialState, action);
      expect(result.ids).toEqual(['1']);
    });
  });

  describe('ordersLoaded', () => {
    it('should sets all', () => {
      const orders = [
        {
          id: '1',
          orderDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
          deliveryDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
          notes: 'Malayan Tapir',
        },
        {
          id: '2',
          orderDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
          deliveryDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
          notes: 'Malayan Tapir',
        }
      ];
      const action = ordersLoaded({ orders });
      const result = reducer(initialState, action);
      expect(result.ids).toEqual(['1', '2']);
    });
  });
});
