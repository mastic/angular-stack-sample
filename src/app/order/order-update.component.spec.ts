import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OrderUpdateComponent } from './order-update.component';
import { SharedModule } from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { RouterModule } from '@angular/router';
import { OrderFormComponent } from './order-form.component';

describe('OrderUpdateComponent', () => {
  let component: OrderUpdateComponent;
  let fixture: ComponentFixture<OrderUpdateComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers), RouterModule.forRoot([], { relativeLinkResolution: 'legacy' })],
      declarations: [OrderUpdateComponent, OrderFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
