import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderComponent } from './order.component';
import { OrderUpdateComponent } from './order-update.component';
import { OrderDeleteDialogComponent } from './order-delete-dialog.component';
import { StoreModule } from '@ngrx/store';
import * as fromOrder from './order.reducer';
import { SharedModule } from '../shared/shared.module';
import { OrderFormComponent } from './order-form.component';
import { EffectsModule } from '@ngrx/effects';
import { OrderEffects } from './order.effects';

@NgModule({
  declarations: [
    OrderComponent,
    OrderFormComponent,
    OrderUpdateComponent,
    OrderDeleteDialogComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    StoreModule.forFeature(fromOrder.ordersFeatureKey, fromOrder.reducer),
    EffectsModule.forFeature([OrderEffects])
  ]
})
export class OrderModule { }
