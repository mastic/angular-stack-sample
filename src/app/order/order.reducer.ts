import { Action, createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Order } from './order.model';
import * as OrderActions from './order.actions';

export const ordersFeatureKey = 'orders';

export interface State extends EntityState<Order> {
  entity?: Order;
}

export const adapter: EntityAdapter<Order> = createEntityAdapter<Order>();

export const initialState: State = adapter.getInitialState({
  // additional entity state properties
});

export const reducer = createReducer(
  initialState,
  on(OrderActions.updateOrder,
    (state, action) => adapter.updateOne(action.update, state)
  ),
  on(OrderActions.deleteOrders,
    (state, action) => adapter.removeMany(action.ids, state)
  ),
  on(OrderActions.orderLoaded,
    (state, action) => {
      return { ...adapter.setOne(action.order, state), entity: action.order }
    }
  ),
  on(OrderActions.ordersLoaded,
    (state, action) => adapter.setAll(action.orders, state)
  ),
);

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();
