import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OrderComponent } from './order.component';
import { SharedModule } from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { OrderFormComponent } from './order-form.component';
import { RouterModule } from '@angular/router';

describe('OrderComponent', () => {
  let component: OrderComponent;
  let fixture: ComponentFixture<OrderComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers), RouterModule.forRoot([], { relativeLinkResolution: 'legacy' })],
      declarations: [OrderComponent, OrderFormComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
