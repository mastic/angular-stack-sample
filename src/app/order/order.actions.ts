import { createAction, props } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { Order } from './order.model';

export const loadOrders = createAction('[Order/API] Load Orders');
export const ordersLoaded = createAction('[Order/API] Orders Loaded Successfully',
  props<{ orders: Order[] }>());

export const loadOrder = createAction('[Order/API] Load Order', props<{ id: string }>());
export const orderLoaded = createAction('[Order/API] Order Loaded Successfully',
  props<{ order: Order }>());

export const createOrder = createAction('[Order/API] Create Order',
  props<{ order: Order }>());

export const deleteOrders = createAction('[Order/API] Delete Orders',
  props<{ ids: string[] }>());

export const updateOrder = createAction('[Order/API] Update Order',
  props<{ update: Update<Order> }>());

export const orderActionTypes = {
  loadOrders,
  ordersLoaded,
  loadOrder,
  orderLoaded,
  createOrder,
  deleteOrders,
  updateOrder,
};
