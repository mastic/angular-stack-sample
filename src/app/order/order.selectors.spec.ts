import { State } from './order.reducer';
import { selectAllOrders } from './order.selectors';

const state: State = {
  ids: ['1', '2', '3'],
  entities: {
    1: { id: '1',
           orderDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
           deliveryDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
           notes: 'Malayan Tapir',
      },
      2: { id: '2',
           orderDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
           deliveryDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
           notes: 'Malayan Tapir',
      },
      3: { id: '3',
           orderDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
           deliveryDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
           notes: 'Malayan Tapir',
      },
  },
};

describe('Order Selectors', () => {
  it('selectAllOrders', () => {
    expect(selectAllOrders.projector(state)).toEqual([
      {
        id: '1',
        orderDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
        deliveryDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
        notes: 'Malayan Tapir',
      },
      {
        id: '2',
        orderDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
        deliveryDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
        notes: 'Malayan Tapir',
      },
      {
        id: '3',
        orderDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
        deliveryDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
        notes: 'Malayan Tapir',
      }
    ]);
  });
});
