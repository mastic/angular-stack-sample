import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, forkJoin } from 'rxjs';
import { Order } from './order.model';
import { Update } from '@ngrx/entity';

@Injectable({
  providedIn: 'root',
})
export class OrderService {
  http: HttpClient;

  constructor(http: HttpClient) {
    this.http = http;
  }

  getAll(): Observable<Order[]> {
    return this.http.get<Order[]>(`/api/orders`);
  }

  getOne(key: string): Observable<Order> {
    return this.http.get<Order>(`/api/orders/${key}`);
  }

  create(order: Order): Observable<Order> {
    return this.http.post<Order>(`/api/orders`, order);
  }

  delete(keys: string[]): Observable<any> {
    return forkJoin(keys.map(key => this.http.delete(`/api/orders/${key}`)));
  }

  update(update: Update<Order>): Observable<any> {
    return this.http.put(`/api/orders/${update.id}`, update.changes);
  }
}
