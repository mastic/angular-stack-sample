import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Order } from './order.model';

@Component({
  selector: 'app-order-delete-dialog',
  templateUrl: './order-delete-dialog.component.html',
  styleUrls: ['./order-delete-dialog.component.sass'],
})
export class OrderDeleteDialogComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {selection: Order[]},
    public dialogRef: MatDialogRef<OrderDeleteDialogComponent>
  ) {}

  ngOnInit(): void {}
}
