import { TestBed } from '@angular/core/testing';

import { OrderService } from './order.service';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../reducers';
import { SharedModule } from '../shared/shared.module';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('OrderService', () => {
  let service: OrderService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot(reducers), HttpClientTestingModule],
    });
    service = TestBed.inject(OrderService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  const orders = [
        {
          id: '1',
          orderDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
          deliveryDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
          notes: 'Malayan Tapir',
        },
        {
          id: '2',
          orderDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
          deliveryDate: new Date(Date.UTC(2020, 1, 0, 9, 0)),
          notes: 'Malayan Tapir',
        }
  ];

  describe('#getAll', () => {
    it('should return an Observable<Order[]>', () => {
      service.getAll().subscribe((result) => {
        expect(result.length).toBe(2);
        expect(result).toEqual(result);
      });

      const req = httpMock.expectOne(`/api/orders`);
      expect(req.request.method).toBe('GET');
      req.flush(orders);
    });
  });

  describe('#getOne', () => {
    it('should return an Observable<Order>', () => {
      service.getOne('42').subscribe((result) => {
        expect(result).toBeTruthy();
        expect(result).toEqual(result);
      });

      const req = httpMock.expectOne(`/api/orders/42`);
      expect(req.request.method).toBe('GET');
      req.flush(orders);
    });
  });

  describe('#create', () => {
    it('should return an Observable<Order>', () => {
      service.create(orders[0]).subscribe((result) => {
        expect(result).toBeTruthy();
        expect(result).toEqual(result);
      });

      const req = httpMock.expectOne(`/api/orders`);
      expect(req.request.method).toBe('POST');
      req.flush(orders);
    });
  });

  describe('#delete', () => {
    it('should return an Observable<Order>', () => {
      service.delete(['42']).subscribe((result) => {
        expect(result).toBeTruthy();
        expect(result).toEqual(result);
      });

      const req = httpMock.expectOne(`/api/orders/42`);
      expect(req.request.method).toBe('DELETE');
      req.flush(orders);
    });
  });

  describe('#update', () => {
    it('should return an Observable<Order>', () => {
      service.update({id: '42', changes: {}}).subscribe((result) => {
        expect(result).toBeTruthy();
        expect(result).toEqual(result);
      });

      const req = httpMock.expectOne(`/api/orders/42`);
      expect(req.request.method).toBe('PUT');
      req.flush(orders);
    });
  });

});
