FROM nginx:stable
ARG app

COPY nginx.conf /etc/nginx/nginx.conf
COPY dist/${app}/ /usr/share/nginx/html
