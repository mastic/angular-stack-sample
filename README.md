# Angular Stack Sample

This project was generated with [mastic](https://mastic.gitlab.io/) version 0.2.0.

## Model

```plantuml

class Customer {
  + String name
  + Order orders
}



class Order {
  + datetime orderDate
  + datetime deliveryDate
  + String notes?
  + Customer customer
  + OrderItem items
}
Order "*" *--o "1" Customer : customer


class Product {
  + String name
  + String code
  + OrderItem orders
}



class OrderItem {
  + int amount
  + Order order
  + Product product
}
OrderItem "*" *--o "1" Order : order
OrderItem "*" *--o "1" Product : product



```

[//]: # "mastic-pin-readme"

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
